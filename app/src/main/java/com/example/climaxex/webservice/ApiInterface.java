package com.example.climaxex.webservice;

import com.example.climaxex.model.Bookmark;
import com.example.climaxex.model.Chapter;
import com.example.climaxex.model.Story;
import com.example.climaxex.model.User;
import com.example.climaxex.model.Category;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    String storyID = "";
    String chapterID = "";
    String categoryName = "";
    String pageNum = "";

    @GET("user")
    Call<User> retrieveUser();

    @GET("validRequest")
    Call<String> validateHeader();
    @PATCH("user/"+"/oneSignalId")
    Call<User> updateOneSignalID(@Path("userId") String id,@Body User user);

    @GET("user/"+"/bookmark")
    Call<List<Bookmark>>retrieveAllBookmarks();
    @POST("user/"+"/bookmark")
    Call<Bookmark>updateBookmark(@Body Bookmark bookmark);

    @GET("category")
    Call<List<Category>>retrieveCategories();

    //need to pass variables

    @GET("story/index/"+ categoryName+"/"+pageNum+"")
    Call<List<Story>>retrieveStories();

    @GET("story/"+storyID)
    Call<Story>retrieveStory();

    @GET("story/"+storyID+"/"+chapterID)
    Call<Chapter>retrieveChapter();

    @GET("membershipDetails")
    Call<JsonObject>retrieveMembershipDetails();


}
