package com.example.climaxex.model;

public class UserObject {
    private int id = 0;
    private String userID = "";
    private String oneSingnalID = "";

    public UserObject(int id, String userID, String oneSingnalID) {
        this.id = id;
        this.userID = userID;
        this.oneSingnalID = oneSingnalID;
    }

    public UserObject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getOneSingnalID() {
        return oneSingnalID;
    }

    public void setOneSingnalID(String oneSingnalID) {
        this.oneSingnalID = oneSingnalID;
    }
}
