package com.example.climaxex;

import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.security.KeyChain;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.example.climaxex.adapter.RecyclerViewAdapter;
import com.example.climaxex.model.Bookmark;
import com.example.climaxex.model.Category;
import com.example.climaxex.model.Chapter;
import com.example.climaxex.model.ItemObject;
import com.example.climaxex.model.Story;
import com.example.climaxex.model.User;
import com.example.climaxex.webservice.ApiInterface;
import com.example.climaxex.webservice.ServiceGenerator;
import com.google.gson.JsonObject;

import com.example.climaxex.activity.SettingsActivity;
import com.example.climaxex.activity.SortActivity;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;


public class MainActivity extends AppCompatActivity {
    private TextView textView;
    private StaggeredGridLayoutManager _sGridLayoutManager;
    //  private TextView textView = findViewById(R.id.test_text);
    //"https://api.myjson.com/bins/1887lc"
    private static final String rootUrl ="https://api.climax.ly/user/";
    private static final String key = "7ada1f23292f0d304fbb4dbead500f6c";
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String textResponse = "";
    private static String userId = "shohad";
    private static User currentUser = new User();
    private static String validHeader = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) this.findViewById(R.id.textView);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

















        ///////////////////////////////////////////////////////UI/////////////////////////////////////////////////////////////////////////////////////////////////////////
        Button optionButton = findViewById(R.id.optionButton);
        optionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openSettings();
            }
        });
        Button sortButton = findViewById(R.id.sortButton);
        sortButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openSortMenu();
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        _sGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(_sGridLayoutManager);

        List<ItemObject> sList = getListItemData();

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(
                MainActivity.this, sList);
        recyclerView.setAdapter(rcAdapter);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void retrieveUser(){

        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<User> call = apiInterface.retrieveUser();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                currentUser = response.body();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("getUserFailure", t.getMessage());
            }
        });

    }

    private void validateRequest(){

        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<String> call = apiInterface.validateHeader();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                validHeader = response.body();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }

    private void updateOneSignalID(String userId){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);


    }

    private void retrieveAllBookmarks(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<List<Bookmark>> call = apiInterface.retrieveAllBookmarks();
        call.enqueue(new Callback<List<Bookmark>>() {
            @Override
            public void onResponse(Call<List<Bookmark>> call, Response<List<Bookmark>> response) {

            }

            @Override
            public void onFailure(Call<List<Bookmark>> call, Throwable t) {

            }
        });
    }

    private void updateBookmark(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
       // Call<Bookmark> call = apiInterface.updateBookmark();
    }


    private void retrieveCategories(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<List<Category>> call = apiInterface.retrieveCategories();
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });
    }


    private void retrieveStories(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<List<Story>> call = apiInterface.retrieveStories();
        call.enqueue(new Callback<List<Story>>() {
            @Override
            public void onResponse(Call<List<Story>> call, Response<List<Story>> response) {

            }

            @Override
            public void onFailure(Call<List<Story>> call, Throwable t) {

            }
        });
    }


    private void retrieveStory(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<Story> call = apiInterface.retrieveStory();
        call.enqueue(new Callback<Story>() {
            @Override
            public void onResponse(Call<Story> call, Response<Story> response) {

            }

            @Override
            public void onFailure(Call<Story> call, Throwable t) {

            }
        });
    }

    private void retrieveChapter(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<Chapter> call = apiInterface.retrieveChapter();
        call.enqueue(new Callback<Chapter>() {
            @Override
            public void onResponse(Call<Chapter> call, Response<Chapter> response) {

            }

            @Override
            public void onFailure(Call<Chapter> call, Throwable t) {

            }
        });
    }


    private void retrieveMembershipDetails(){
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<JsonObject> call = apiInterface.retrieveMembershipDetails();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    private void openSortMenu() {

        Intent intent = new Intent(this,SortActivity.class);
        startActivity(intent);
    }

    private void openSettings() {

        Intent intent = new Intent(this,SettingsActivity.class);
        startActivity(intent);
    }

    private String retrieveKeychainUserID(){

        return null;
    }





    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private List<ItemObject> getListItemData()
    {
        List<ItemObject> listViewItems = new ArrayList<ItemObject>();


        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        listViewItems.add(new ItemObject("Placeholder2342398239", "Placeholder2342398239"));
        listViewItems.add(new ItemObject("Placeholder", ""));
        return listViewItems;
    }







}
