package com.example.climaxex.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.climaxex.MainActivity;
import com.example.climaxex.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        Button returnButton = findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnNavigate();
            }
        });

        TextView reviewText = findViewById(R.id.reviewText);
        reviewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewUsNavigate();
            }
        });


        TextView termsText = findViewById(R.id.termsText);
        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                termsNavigate();
            }
        });

        TextView faqButton = findViewById(R.id.faqText);
        faqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                faqNavigate();
            }
        });


    }

    private void returnNavigate() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    private void reviewUsNavigate(){

        Intent intent = new Intent(this, ReviewUsActivity.class);
        startActivity(intent);
    }

    private void faqNavigate(){
        Intent intent = new Intent(this, FaqActivity.class);
        startActivity(intent);
    }

    private void termsNavigate(){

        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }
}
