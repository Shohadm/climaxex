package com.example.climaxex.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.climaxex.MainActivity;
import com.example.climaxex.R;
import com.example.climaxex.activity.FaqActivity;
import com.example.climaxex.activity.RomanceActivity;

public class SortActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort);


        Button cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSort();
            }
        });


    }

    private void cancelSort() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);


    }

    public void openPopularSort(View view){

        Intent intent = new Intent(this,SubscriptionActivity.class);
        startActivity(intent);

    }
    public void openRomanceSort(View view){

        Intent intent = new Intent(this,RomanceActivity.class);
        startActivity(intent);

    }
    public void openHorrorSort(View view){

        Intent intent = new Intent(this,HorrorActivity.class);
        startActivity(intent);

    }


}
